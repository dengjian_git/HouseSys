<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	input{
		text-align: top;
	}
</style>
	<link rel="stylesheet" href="css/bootstrap.min.css">  
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript">
		function load(){
			$.getJSON("districtList",function(data){
				var html="<table class='table table-striped' style='width:900px;'><caption>地区管理</caption><thead><tr><th>编号</th><th>地区</th><th>操作</th></tr></thead>";
				$.each(data.districts,function(i,item){
					html+="<tbody><tr>";
					html+="<td>"+item["id"]+"</td>";
					html+="<td>"+item["name"]+"</td>";
					html+= "<td><a href='#' data='"+item["id"]+"' class='update'>查看</a>";   
					html+="<a href='#' data='"+item["id"]+"' class='del'>删除</a></td></tr></tbody>";
				});
				html+="<tr><td colspan='3'><ul class='pagination' style='margin-left:300px;'>";
				html+="<li><a href='?pageNo="+(data.pageNo-1)+"'>&laquo;</a></li>";
				html+="<li class='active'><a href='#'>1</a></li>";
				html+="<li class='disabled'><a href='#'>2</a></li>";
				html+="<li><a href='?pageNo="+(data.pageNo+1)+"'>&raquo;</a></li>";
				html+="<li style='margin-left:40px;margin-top:6px;display:inline-block;' >第 "+data.pageNo+"页，共"+data.totalPages+"页</li>";
				html+="</ul></td>";
				html+="</tr>";
				html+="</table>";
				$("#result").html(html);
			})
		}
		//加载数据
		$(function(){
			load();
		});
	</script>
</head>
<body>
	搜索 <input type="text" name="name" id="name"/><input type="button" id="soubut" value="查询"/>
	<div id="result">
		
	</div>
</body>
</html>