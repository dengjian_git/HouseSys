<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="css/page.css"/>
</head>
<body>
	<div id="page">
		<!-- 导入头 -->
		<tiles:insertAttribute name="header"/>
		<div id="content">
			<div id="nav">
				<c:import url="/nav"/>
			</div>
			<div id="default_main">
				<!-- 导入数据 -->
				<tiles:insertAttribute name="main"/>
			</div>
			<div id="clean">
				<!-- 导入尾 -->
				<tiles:insertAttribute name="footer"/>
			</div>
		</div>
	</div>
</body>
</html>