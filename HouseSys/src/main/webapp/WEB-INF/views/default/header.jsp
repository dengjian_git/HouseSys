<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA_Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insert title here</title>
<link href="css/style.css" rel="stylesheet">
<style type="text/css">
	#page_header{
		background-image: url(images/log.png);
		background-repeat: no-repeat;
	}
</style>
<link rel="stylesheet" href="css/bootstrap.min.css">  
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myfunction.js"></script>

</head>
<body>
	<div id="header" style="background-image: url(images/log.png);background-repeat: no-repeat;background-size:620px 66px;">
			<ul class="nav nav-pills" style="padding-left: 311px;margin-top: 14px;">
			    <li class="active"><a href="#">Home</a></li>
			    <li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="district_ajax">
				             地区管理<span class="caret"></span>
				    </a>
				    <ul class="dropdown-menu">
				        <li><a href="#" id="add">增加地区</a></li>
				        <li><a href="#">删除地区</a></li>
				        <li><a href="#">修改地区</a></li>
				        <li><a href="district_ajax">查询地区</a></li>
				        <li class="divider"></li>
				        <li><a href="#">分离的链接</a></li>
			      	</ul>
				</li>
			    <li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="street_ajax">
				             街道管理<span class="caret"></span>
				    </a>
				    <ul class="dropdown-menu">
				        <li><a href="#" id="streetAdd">增加街道</a></li>
				        <li><a href="#">删除街道</a></li>
				        <li><a href="#">修改街道</a></li>
				        <li><a href="street_ajax">查询街道</a></li>
				        <li class="divider"></li>
				        <li><a href="#">分离的链接</a></li>
			      	</ul>
				</li>
			    <li class="dropdown">
			        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
			       	     房型管理 <span class="caret"></span>
			        </a>
			        <ul class="dropdown-menu">
				        <li><a href="#" id="type_add">增加房型 </a></li>
				        <li><a href="#">删除房型 </a></li>
				        <li><a href="#">修改房型 </a></li>
				        <li><a href="type_ajax">查询房型 </a></li>
				        <li class="divider"></li>
				        <li><a href="#">分离的链接</a></li>
		      	    </ul>
			    </li>
			    <li class="dropdown">
			        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
			       	   违规房屋 <span class="caret"></span>
			        </a>
			        
			    </li>
			    <li class="dropdown">
			        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
			       	   违规用户 <span class="caret"></span>
			        </a>
			        
			    </li>
			  </ul>
			  <span id="show"></span>
			  <a href="#" style="text-align: right;float:right;margin-right:120px;">用户名:</a>
		</div>
</body>
</html>