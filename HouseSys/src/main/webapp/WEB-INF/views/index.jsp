<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="mobile-agent" content="format=html5; url=https://m.anjuke.com/gz/rent/" />
<link href="https://gz.zu.anjuke.com/" rel="canonical">
<title>租房网</title><meta name="keywords" content="">
<meta name="description" content="租房网为您提供广州房屋出租信息和广州租房价格,包括广州二手房出租、新房出租，租房子,找广州出租房,就上租房网。">
<meta name="mobile-agent" content="format=html5; url=https://m.anjuke.com/gz/rent/" />
    <meta name="baidu-site-verification" content="e8abd676df9f995bc969ac138b1c0f4d"/>
    <meta name="sogou_site_verification" content="7rtgKfBjbl"/>
    <meta name="360-site-verification" content="f7b8b308108b2c1c2de2825948822256" />
    <meta name="google-site-verification" content="drkSj5A3WGSgkMXwzh6UfezwLEMsEXoQlMHL25oE1kA" />
    <meta baidu-gxt-verify-token="9e7961d9a5d01603e5c2ae9bccffb9c2"/>
    <meta name="shenma-site-verification" content="da9c53da88979ec98afae25b1ca3e43b" />
    <link rel="stylesheet" rev="stylesheet" href="css/index.css" type="text/css" />
    <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/myfunction.js"></script>
    </head>
<body>
     
<div class="header-center none">
        </div>



<div class="topbar ">
    <div class="header-center clearfix">
        <ul class="nav-site clearfix">
			<li class="first" ><a href="/" _soj="navigation">首 页</a></li>
			<li  ><a href="" _soj="navigation">新 房</a></li>
			<li  ><a href="" _soj="navigation">二手房</a></li>
			<li  ><a href="" _soj="navigation">租 房</a></li>
			<li  ><a href="" _soj="navigation">商铺写字楼</a></li>
			<li  ><a href="" _soj="navigation">海外地产</a></li>
			<li  ><a href="" _soj="navigation">楼讯</a></li>
			<li  ><a href="" _soj="navigation">房 价</a></li>
			<li  ><a href="" _soj="navigation">问 答</a></li>
		</ul>
        <div id="userbox" class="user">
            <!-- 用户登录 -->
            <div class="userlogin userblock">
                <i class="icon icon-people"></i>
                <a class="link" href="" rel="nofollow" id="user_login">登录</a>
                <a class="link" href="" rel="nofollow" id="user_register">注册</a>
            </div>
		
        </div>
		<div id="user_div">
			<h3>用户登录</h3>
			<span id="user_span">x</span>
			<form action="" method="get">
				<input style="width:200px;height:30px;margin-left:88px;margin-top:10px;" type="text" name="" id=""/><br/>
				<input style="width:200px;height:30px;margin-left:88px;margin-top:10px;" type="password" name="" id=""/><br/>
				<input  style="width:75px;height:30px;margin-left:93px;margin-top:10px;" type="submit" value="登录" />
				<input  style="width:75px;height:30px;margin-left:35px;margin-top:10px;" type="submit" value="注册" />
			</form>
		</div>
		<div id="user_register_div">
			<h3>用户注册</h3>
			<span id="user_span">x</span>
			<form action="" method="get">
				<input style="width:200px;height:30px;margin-left:88px;margin-top:10px;" type="text" name="" id=""/><br/>
				<input style="width:200px;height:30px;margin-left:88px;margin-top:10px;" type="password" name="" id=""/><br/>
				<input  style="width:75px;height:30px;margin-left:93px;margin-top:10px;" type="submit" value="登录" />
				<input  style="width:75px;height:30px;margin-left:35px;margin-top:10px;" type="submit" value="注册" />
			</form>
		</div>
    </div>
</div> <!-- topbar -->

<div class="header header-center clearfix">
    <a class="logo" href="#" title="">租房网</a>
    <a class="logo-site">租房</a>
        <div class="cityselect">
        <div id="switch_apf_id_5" class="city-view" >
            广州<i class="iconfont triangle-down">&#xE030;</i>
        </div>
        <div id="city_list" class="city-list" style="display:none;">
                
            <dl>
                <dt>华南地区</dt>
                <dd>
					<a href=" " title="深圳租房网">深圳</a>
					<a href=" " title="广州租房网">广州</a>
					<a href=" " title="佛山租房网">佛山</a>
					<a href=" " title="长沙租房网">长沙</a>
					<a href=" " title="三亚租房网">三亚</a>
					<a href=" " title="惠州租房网">惠州</a>
					<a href=" " title="东莞租房网">东莞</a>
					<a href=" " title="海口租房网">海口</a>
					<a href=" " title="珠海租房网">珠海</a>
					<a href=" " title="中山租房网">中山</a>
					<a href=" " title="厦门租房网">厦门</a>
					<a href=" " title="南宁租房网">南宁</a>
					<a href=" " title="泉州租房网">泉州</a>
					<a href=" " title="柳州租房网">柳州</a>
				</dd>
            </dl>
            <div class="morecity"><a href="https://www.anjuke.com/sy-city.html">更多城市&gt;</a></div>
        </div>
    </div>
            <form class="search-form" id ="search-form" action="" target="_self"  method="GET">
    <input type="hidden" name='t'value="1"/>
    <input type="hidden" name="from" value="0"/>
	<input type='hidden' name='comm_exist' value="on"/>
	<input type="text" name="kw" class="searchbar-rent" id="search-rent" placeholder="请输入小区名称、地址…" autocomplete="off" maxlength="100" value=""><input type="submit" id="search-button" class="searchbar-button" hidefocus="true" value="搜索">
	<i id="search-close" class="icon icon-close" data-tracker="delete-kw" style="display:none"></i>
            <a href="https://gz.zu.anjuke.com/ditu/?from=searchbar" rel="nofollow" class="search-map" target="_blank" _soj="searchbar"><i class="icon icon-mark"></i>地图找房</a>
    </form>

    </div>

<div class="nav header-center clearfix">
	<ul>
		<li><a href="https://gz.zu.anjuke.com/" _soj="navigation" class=" current "  >区域找房</a></li>
		<li><a href="https://gz.zu.anjuke.com/ditu/" _soj="navigation" class="  " target="_blank" >地图找房</a></li>
		<li>
			<a href="javascript:void(0)" _soj="navigation" class="" id="toRent" style="background-color:">我要出租
				<div class="personal-rent-dialog">
				<span class="arrow_box">
					<i></i>
					<em></em>
				</span>
					<img class="link" src="https://pages.anjukestatic.com/usersite/site/img/head/rent_publish.png">
					<p class="tip">扫码免费发布个人租房</p>
					<p class="tip">百万用户，等您出租</p>
				</div>
			</a>
		</li>
        
    </ul>

</div>


    <div class="w1180">
    <div class="breadcrumbs">
                    <div class="p_1180 p_crumbs">
        <a href="https://guangzhou.anjuke.com">广州房产网</a> &gt; <a href="https://gz.zu.anjuke.com">广州租房</a>    </div>
  <!-- 面包屑组件 -->
    </div>
        <div class="div-border items-list">
                <!-- 区域 begin-->
                    <div class="items">
                <span class="item-title">位置：</span>
                <span class="elems-l">
                    <a href=" " title="区域租房" class="selected-item">区域</a>
                    <a href=" " title="地铁租房" class="">地铁</a>
                    <div class="sub-items sub-level1">
                        <em class="arrow-wrap ">
							<em class="arrow"></em>
						</em>
                    <a href=" " title="全部租房" class="selected-item">全部</a>
					<a href=" " title="番禺租房" class="">番禺</a>
					<a href=" " title="天河租房" class="">天河</a>
					<a href=" " title="白云租房"  class="">白云</a>
					<a href=" " title="海珠租房" class="">海珠</a>
					<a href=" " title="增城租房" class="">增城</a>
					<a href=" " title="花都租房" class="">花都</a>
					<a href=" " title="越秀租房" class="">越秀</a>
					<a href="" title="黄埔租房" class="">黄埔</a>
					<a href=" " title="南沙租房" class="">南沙</a>
					<a href=" " title="荔湾租房"  class="">荔湾</a>
					<a href=" " title="从化租房" class="">从化</a>
					<a href=" " title="广州周边租房" class="">广州周边</a>
                 </span>
            </div>
                <!-- 区域 end-->

        <!-- 租金 begin-->
                    <div class="items ">
                <span class="item-title">租金：</span>
                <span class="elems-l">
                    <a href="#"  class="selected-item" rel="nofollow" >全部</a>
                    <a href="#"  class="" rel="nofollow" >1000元以下</a>
                    <a href="#" class="" rel="nofollow" >1000-1500元</a>
                    <a href="#" class="" rel="nofollow" >1500-2000元</a>
                    <a href="#" class="" rel="nofollow" >2000-2500元</a>
                    <a href="#"  class="" rel="nofollow"  >2500-3000元</a>
                    <a href="#" class="" rel="nofollow" >3000-3500元</a>
                    <a href="#" class="" rel="nofollow" >3500-4000元</a>
                    <a href="#" class="" rel="nofollow" >4000-5000元</a>
                    <a href="#" class=""  rel="nofollow"  >5000-6000元</a>
                    <a href="#"  class="" rel="nofollow" >6000-8000元</a>
                    <a href="#" class="" rel="nofollow" >8000元以上</a>
                                        <div class="pricecond">
                        <!--两个输入框的长度width需要在加载时传入参数计算：数字长度小于等于3位时，width:24px\
                            数字长度n大于3位时，width: 8*n px-->
                        <form action="#" id="price_range_form" onsubmit="">
                            <input
                                id="from-price"
                                class="from-price"
                                type="text"
                                name="from_price"
                                maxlength="5"
                                value=""
                                style="width:32px;"
                                autocomplete="off"
                            /> -
                            <input
                                id="to-price"
                                class="to-price"
                                type="text"
                                name="to_price"
                                maxlength="5"
                                value=""
                                style="width:32px;"
                                autocomplete="off"
                            />&nbsp;<span class="">元</span>
                               <input class="smit" id="pricerange_search" style="" type="button" value="确定">
                        </form>
                    </div>
                </span>
            </div>
        
        <!--  房型 begin-->
                    <div class="items">
                <span class="item-title">房型：</span>
                <span class="elems-l">
                                            <a href=" "
                           class="selected-item"
                           rel="nofollow"
                        >全部</a>
                                            <a href=" "
                           class=""
                           rel="nofollow"
                        >一室</a>
                                            <a href=" "
                           class=""
                           rel="nofollow"
                        >二室</a>
                                            <a href=" "
                           class=""
                           rel="nofollow"
                        >三室</a>
                                            <a href=" "
                           class=""
                           rel="nofollow"
                        >四室</a>
                                            <a href=" "
                           class=""
                           rel="nofollow"
                        >五室及以上</a>
                                    </span>
            </div>
        
                    <div class="items">
                <span class="item-title">类型：</span>
                <span class="elems-l">
                                            <a href=" "
                           class="selected-item"
                           rel="nofollow"
                        >全部</a>
                                            <a href=" "
                           class=""
                           rel="nofollow"
                        >整租</a>
                                            <a href=" "
                           class=""
                           rel="nofollow"
                        >合租</a>
                                    </span>
            </div>
    <!--主模块-->
    <div class="maincontent">
        <div class="list-content" id="list-content">
            <!--标签-->
            <div class="zu-tab">
                <a href=" "
                   class="curTab">全部
                </a>
                   <a class="lastTab">经纪人房源
                </a>

                    <a href=" "
                   class="lastTab">个人房源
                </a>
         </div>
            <!--排序-->
            <div class="zu-sort">
                <span class="tit">为您找到<em>广州</em>附近的租房</span>
                <div class="sort-cond">
                                        <span>排序 ：</span>
                    <a
                        href=" "
                        class="light"
                    >默认
                    </a>
                    <a
                        href=" "
                        class=""
                    >租金<i class="icon icon-arrup"></i></a>
                    <a
                        href=" "
                        class=""
                    >最新<i class="icon icon-arrdown"></i></a>
                    <!--icon-arrup-org icon-arrdown-org为高亮箭头-->
                </div>
            </div>

            <!--房源列表豆腐块-->
            <!--区域板块租房房源列表页-->
              <div class="zu-itemmod  "  link=" " _soj="Filter_1&hfilter=filterlist">
                    <a
                        data-company=""
                        class="img"
                        _soj="Filter_1&hfilter=filterlist"
                        data-sign="true"
                        href=" "
                        title="加怡花园东晓南精装修 无隔断  押一付一 家电齐全"
                        alt="加怡花园东晓南精装修 无隔断  押一付一 家电齐全"
                        target="_blank"
                        hidefocus="true"
                    >
                        <img
                            class="thumbnail"
                            src=" "
                            alt="加怡花园东晓南精装修 无隔断  押一付一 家电齐全"
                            width="180"
                            height="135"
                        />
                          <span class="many-icons iconfont">&#xE062;</span>
                     </a>
                    <div class="zu-info">
                        <h3>
                            <a
                                target="_blank"
                                title="加怡花园东晓南精装修 无隔断  押一付一 家电齐全"
                                _soj="Filter_1&hfilter=filterlist"
                                href=" "
                            >加怡花园东晓南精装修 无隔断  押一付一 家电齐全</a>                        </h3>
                        <p class="details-item tag">
                            2室0厅<span>|</span>29平米<span>|</span>22/26层<i class="iconfont jjr-icon">&#xE147;</i>李涓                        </p>
                        <address class="details-item">
                                                            <a target="_blank"
                                   href=" ">加怡花园</a>&nbsp;&nbsp;
                                                        海珠-东晓南 侨港路                        </address>
                        <p class="details-item bot-tag clearfix">
                            <span class="cls-1">整租</span>
                            <span class="cls-2">东南</span>
                                                            <span class="cls-3">2号线</span>
                                                    </p>
                    </div>
                    <div class="zu-side">
                        <p><strong>2330</strong> 元/月</p>
                    </div>
                </div>
                                       
                                           
                     
                
                        <!--零少结果推荐文案,置于最底部-->
            <div id="zu-comhead" class="zu-comhead" style="display:none;">
                <p>哎呀，没有找到符合要求的房子。</p>
                <p class="mid">安居客建议：看看上面的筛选条件是否合理</p>
                <em>根据您的租房要求，特别推荐以下房源</em>
            </div>
</body>
</html>