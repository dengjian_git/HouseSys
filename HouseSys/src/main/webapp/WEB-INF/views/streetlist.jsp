<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	input{
		text-align: top;
	}
</style>
	<script type="text/javascript">
		function load(){
			$.getJSON("streetQueryAll",function(data){
				var html="<table class='table table-striped' style='width:900px;'><caption>地区管理</caption><thead><tr><th>编号</th><th>街道</th><th>所属地区</th><th>操作</th></tr></thead>";
				$.each(data,function(i,item){
					html+="<tbody><tr>";
					html+="<td>"+item["id"]+"</td>";
					html+="<td>"+item["name"]+"</td>";
					html+="<td>"+item["districtId"]+"</td>";
					html+= "<td><a href='#' data='"+item["id"]+"' class='update'>查看</a>&nbsp;&nbsp;";   
					html+="<a href='#' data='"+item["id"]+"' class='del'>删除</a></td></tr></tbody>";
				});
				html+="</table>";
				$("#result").html(html);
			});
		}
		function street_district_load(){
			$.getJSON("street_districtId",function(data){
				var html="";
				$.each(data,function(i,item){
					html+="<option value='"+item["id"]+"'>"+item["name"]+"</option>";
				});
				$("#districtId").html(html);
			});
		}
		//加载数据
		$(function(){
			load();
			street_district_load();
		});
		$(function(){
			//增加
			$("#streetAdd").click(function(){
				$("#add_street_div").css({"display":"block"});
			});
			$("#save_street_but").click(function(){
				var name=$("#streetName").val();
				var districtId = $("#districtId").val();
				$.post("addStreet",{"name":name,"districtId":districtId},function(data){
					alert("add");
					load();
				});
				$("#add_street_div").css({"display":"none"});
			});
			$("#quxiao_street_but").click(function(){
				$("#add_street_div").css({"display":"none"});
			});
			//删除
			$("#result").on("click",".del",function(){
				var id=$(this).attr("data");
				if(confirm("确认删除吗?")){
					$.get("deleteStreet",{"id":id},function(data){
						load();
					})	
				}
			});
		});
		//修改
		$(function(){
			$("#result").on("click",".update",function(){
				var id=$(this).attr("data");
				$("#street_update_div").css({"display":"block"});
				$.getJSON("street_districtId",function(data){
					var html="";
					$.each(data,function(i,item){
						html+="<option value='"+item["id"]+"'>"+item["name"]+"</option>";
					});
					$("#update_districtId").html(html);
				});
				/* $.getJSON("street_byId",{"id":id},function(data){
					var nameVal="";
					$.each(data,function(i,item){
						nameVal+=item["name"];
					});
					alert(nameVal);
					$("#udpate_streetName").val(nameVal);
				}); */
				$("#update_street_but").click(function(){
					var name=$("#udpate_streetName").val();
					var districtId = $("#update_districtId").val();
					$.post("updateStreet",{"id":id,"name":name,"districtId":districtId},function(data){
						load();
					});	
					$("#street_update_div").css({"display":"none"});
				});
				$("#up_qx_but").click(function(){
					 $("#street_update_div").css({"display":"none"});
				});
			});
			
		});
		//搜索
		$(function(){
			$("#soubut").click(function(){
				var name = $("#so_street_name").val();
				$.getJSON("streetList",{"name":name},function(data){
					var html="<table class='table table-striped' style='width:900px;'><caption>街道管理</caption><thead><tr><th>编号</th><th>地区</th><th>操作</th></tr></thead>";
					$.each(data.districts,function(i,item){
						html+="<tbody><tr>";
						html+="<td>"+item["id"]+"</td>";
						html+="<td>"+item["name"]+"</td>";
						html+="<td>"+item["districtId"]+"</td>";
						html+= "<td><a href='#' data='"+item["id"]+"' class='update'>查看</a>&nbsp;&nbsp;";   
						html+="<a href='#' data='"+item["id"]+"' class='del'>删除</a></td></tr></tbody>";
					});
					html+="</table>";
					$("#result").html(html);
				});
			});
		});
	</script>
	
</head>
<body>
	<div id="so">搜索 <input type="text" name="so_street_name" id="so_street_name"/><input type="button" id="soubut" value="查询"/></div>
	<div id="result">
	</div>
	<div id="add_street_div">
		<h4>增加街道</h4>
		<input style="margin-left:49px;width:200px;" type="text" name="streetName" id="streetName"/><br/>
		<select id="districtId" name="districtId">
		</select>
		<br/>
		<input style="margin-left:95px;margin-top:10px;" type="button" id="save_street_but" value="保存"/><input style="margin-top:10px;" type="button" id="quxiao_street_but" value="取消"/>
	</div>
	<div id="street_update_div">
		<h4>修改街道</h4>
		<input style="margin-left:49px;width:200px;" type="text" name="udpate_streetName" id="udpate_streetName"/><br/>
		<select id="update_districtId" name="update_districtId">
		</select>
		<br/>
		<input style="margin-left:95px;" type="button" id="update_street_but" value="保存"/><input type="button" id="up_qx_but" value="取消"/>
	</div>
</body>
</html>