<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	input{
		text-align: top;
	}
</style>
	<script type="text/javascript">
		function load(){
			$.getJSON("typeGetAll",function(data){
				var html="<table class='table table-striped' style='width:900px;'><caption>房型管理</caption><thead><tr><th>编号</th><th>房型</th><th>操作</th></tr></thead>";
				$.each(data,function(i,item){
					html+="<tbody><tr>";
					html+="<td>"+item["id"]+"</td>";
					html+="<td>"+item["name"]+"</td>";
					html+= "<td><a href='#' data='"+item["id"]+"' class='update'>查看</a>&nbsp;&nbsp;";   
					html+="<a href='#' data='"+item["id"]+"' class='del'>删除</a></td></tr></tbody>";
				});
				html+="</table>";
				$("#result").html(html);
			});
		}
		//加载数据
		$(function(){
			load();
		});
		$(function(){
			//增加
			$("#type_add").click(function(){
				$("#add_type_div").css({"display":"block"});
			});
			$("#save_but").click(function(){
				var name=$("#typeName").val();
				$.post("addType",{"name":name},function(data){
					alert("add");
					load();
				});
				$("#add_type_div").css({"display":"none"});
			});
			$("#quxiao_but").click(function(){
				$("#add_type_div").css({"display":"none"});
			});
			//删除
			$("#result").on("click",".del",function(){
				var id=$(this).attr("data");
				if(confirm("确认删除吗?")){
					$.get("deleteType",{"id":id},function(data){
						load();
					})	
				}
			});
		});
		//修改
		$(function(){
			$("#result").on("click",".update",function(){
				var id=$(this).attr("data");
				$("#type_update_div").css({"display":"block"});
				$("#update_but").click(function(){
					var name=$("#udpate_typeName").val();
					$.post("updateType",{"id":id,"name":name},function(data){
						load();
					});	
					$("#type_update_div").css({"display":"none"});
				});
			});
		});
		//搜索
		$(function(){
			$("#soubut").click(function(){
				var name = $("#type_Name").val();
				$.getJSON("typeList",{"name":name},function(data){
					var html="<table class='table table-striped' style='width:900px;'><caption>房型管理</caption><thead><tr><th>编号</th><th>房型</th><th>操作</th></tr></thead>";
					$.each(data.types,function(i,item){
						html+="<tbody><tr>";
						html+="<td>"+item["id"]+"</td>";
						html+="<td>"+item["name"]+"</td>";
						html+= "<td><a href='#' data='"+item["id"]+"' class='update'>查看</a>&nbsp;&nbsp;";   
						html+="<a href='#' data='"+item["id"]+"' class='del'>删除</a></td></tr></tbody>";
					});
					html+="</table>";
					$("#result").html(html);
				});
			});
		});
	</script>
	
</head>
<body>
	<div id="so">搜索 <input type="text" name="type_Name" id="type_Name"/><input type="button" id="soubut" value="查询"/></div>
	<div id="result">
	</div>
	<%-- <div>
		<ul class="pagination" style="margin-left:300px">
			<li><a href="?pageNo=${pageNo-1}&name=${param.name}" class="pageNo">&laquo;</a></li>
			<li class="active"><a href="">data.pageNo</a></li>
			<li><a href="?pageNo=${pageNo+1}&name=${param.name}" class="pageNo">&laquo;</a></li>
		</ul>
			
	</div> --%>
	<div id="add_type_div">
		<h4>增加房型</h4>
		<input style="margin-left:49px;width:200px;" type="text" name="typeName" id="typeName"/><br/><br/>
		<input style="margin-left:95px;" type="button" id="save_but" value="保存"/><input type="button" id="quxiao_but" value="取消"/>
	</div>
	<div id="type_update_div">
		<h4>修改房型</h4>
		<input style="margin-left:49px;width:200px;" type="text" name="udpate_typeName" id="udpate_typeName"/><br/><br/>
		<input style="margin-left:95px;" type="button" id="update_but" value="保存"/><input type="button" id="up_qx_but" value="取消"/>
	</div>
</body>
</html>