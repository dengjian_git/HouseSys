package com.house.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.house.biz.DistrictBiz;
import com.house.biz.StreetBiz;
import com.house.entity.District;
import com.house.entity.Street;
@Controller
public class StreetController {
	@Autowired
	private StreetBiz streetBiz;
	@Autowired
	private DistrictBiz districtBiz;
	//跳转页面
	@RequestMapping("/street_ajax")
	public String genre_ajax(){
		return "streetlist";//返回视图名
	}
	//查询所有数据
	@RequestMapping("/streetQueryAll")
	@ResponseBody
	public String getAll(){
		System.out.println(streetBiz.getAll().toString());
		return JSON.toJSONString(streetBiz.getAll());
	}
	//分页
	@RequestMapping(value="/streetList",produces={"application/json;charset=UTF-8"})
	@ResponseBody
	public String getPageIndex(Model model,Integer id,String name,Integer pageNo){
		System.out.println(pageNo);
		id=(id==null)?0:id;
		pageNo=(pageNo==null)?1:pageNo;
		int pageSize=5;
		int startIndex=(pageNo-1)*pageSize;
		List<Street> districts=streetBiz.getPageIndex(name, startIndex, pageSize);
		int rows=streetBiz.getRowsCount(id, name);
		//总页数
		int totalPages=(int)Math.ceil((double)rows/pageSize);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("totalPages",totalPages);
		map.put("name",name);
		map.put("pageNo",pageNo);
		map.put("districts",districts);
		System.out.println(map.toString());
		return JSON.toJSONString(map);
	}
	//返回districtid
	@RequestMapping("/street_districtId")
	@ResponseBody
	public String districtId(){
		return JSON.toJSONString(districtBiz.getAll());
	}
	//增加
	@RequestMapping("/addStreet")
	public void addStreet(Model model,Street street,HttpServletResponse response) throws IOException{
		System.out.println(street.toString());
		System.out.println(districtBiz.getAll().toString());
		model.addAttribute("list", districtBiz.getAll());
		streetBiz.add(street);
		PrintWriter out=response.getWriter();
	}
	//删除数据
	@RequestMapping("/deleteStreet")
	public void deleteStreet(int id,HttpServletResponse response) throws IOException{
		System.out.println("进来了吗");
		streetBiz.delete(id);
		PrintWriter out=response.getWriter();
		out.print("ok");
	}
	@RequestMapping("/street_byId")
	@ResponseBody
	public String getById(int id){
		Street street = new Street();
		street = streetBiz.getById(id);
		return JSON.toJSONString(street);
	}
	//修改数据
	@RequestMapping("/updateStreet")
	public void update(Street street,HttpServletResponse response) throws IOException{
		System.out.println(street.toString());
		streetBiz.update(street);
		PrintWriter out=response.getWriter();
	}
}
