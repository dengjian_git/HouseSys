package com.house.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.house.biz.DistrictBiz;
import com.house.biz.TypeBiz;
import com.house.entity.District;
import com.house.entity.Type;
@Controller
public class TypeController {
	@Autowired
	private TypeBiz typeBiz;
	
	//跳转页面
	@RequestMapping("/type_ajax")
	public String genre_ajax(){
		return "typelist";//返回视图名
	}
	//查询所有数据
	@RequestMapping("/typeGetAll")
	@ResponseBody
	public String getAll(){
		return JSON.toJSONString(typeBiz.getAll());
	}
	//增加
	@RequestMapping("/addType")
	public void addType(Type type,HttpServletResponse response) throws IOException{
		System.out.println(type.toString());
		typeBiz.add(type);
		PrintWriter out=response.getWriter();
	}
	//删除数据
	@RequestMapping("/deleteType")
	public void deleteType(int id,HttpServletResponse response) throws IOException{
		System.out.println("进来了吗");
		typeBiz.delete(id);
		PrintWriter out=response.getWriter();
		out.print("ok");
	}
	//修改数据
	@RequestMapping("/updateType")
	public void update(Type type,HttpServletResponse response) throws IOException{
		System.out.println(type.toString());
		typeBiz.update(type);
		PrintWriter out=response.getWriter();
	}
	
	//分页
	@RequestMapping(value="/typeList",produces={"application/json;charset=UTF-8"})
	@ResponseBody
	public String getPageIndex(Model model,Integer id,String name,Integer pageNo){
		System.out.println(pageNo);
		id=(id==null)?0:id;
		pageNo=(pageNo==null)?1:pageNo;
		int pageSize=5;
		int startIndex=(pageNo-1)*pageSize;
		List<Type> types=typeBiz.getPageIndex(name, startIndex, pageSize);
		int rows=typeBiz.getRowsCount(id, name);
		//总页数
		int totalPages=(int)Math.ceil((double)rows/pageSize);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("totalPages",totalPages);
		map.put("name",name);
		map.put("pageNo",pageNo);
		map.put("types",types);
		System.out.println(map.toString());
		return JSON.toJSONString(map);
	}
}
