package com.house.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
public class AccountController {
	//局部异常处理，处理当前控制器内的异常
		//@ExceptionHandler(value={RuntimeException.class})
		public String  handleException(Exception ex, HttpServletRequest request)
		{
			System.out.println(ex.getMessage());
			request.setAttribute("error", ex);
			return "error";  //error视图
		}
	@RequestMapping("/")
	public String home(){
		return "index";
	}
}
