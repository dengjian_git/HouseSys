package com.house.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.house.biz.DistrictBiz;
import com.house.entity.District;
@Controller
public class DistrictController {
	@Autowired
	private DistrictBiz districtBiz;
	
	//跳转页面
	@RequestMapping("/district_ajax")
	public String genre_ajax(){
		return "districtlist";//返回视图名
	}
	//查询所有数据
	@RequestMapping("/queryAll")
	@ResponseBody
	public String getAll(){
		return JSON.toJSONString(districtBiz.getAll());
	}
	//分页
	@RequestMapping(value="/districtList",produces={"application/json;charset=UTF-8"})
	@ResponseBody
	public String getPageIndex(Model model,Integer id,String name,Integer pageNo){
		System.out.println(pageNo);
		id=(id==null)?0:id;
		pageNo=(pageNo==null)?1:pageNo;
		int pageSize=5;
		int startIndex=(pageNo-1)*pageSize;
		List<District> districts=districtBiz.getPageIndex(name, startIndex, pageSize);
		int rows=districtBiz.getRowsCount(id, name);
		//总页数
		int totalPages=(int)Math.ceil((double)rows/pageSize);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("totalPages",totalPages);
		map.put("name",name);
		map.put("pageNo",pageNo);
		map.put("districts",districts);
		System.out.println(map.toString());
		return JSON.toJSONString(map);
	}
	//增加
	@RequestMapping("/addDistrict")
	public void addDistrict(District district,HttpServletResponse response) throws IOException{
		System.out.println(district.toString());
		districtBiz.add(district);
		PrintWriter out=response.getWriter();
	}
	//删除数据
	@RequestMapping("/delete")
	public void deleteGenre(int id,HttpServletResponse response) throws IOException{
		System.out.println("进来了吗");
		districtBiz.delete(id);
		PrintWriter out=response.getWriter();
		out.print("ok");
	}
	//修改数据
	@RequestMapping("/updateDistrict")
	public void update(District district,HttpServletResponse response) throws IOException{
		System.out.println(district.toString());
		districtBiz.update(district);
		PrintWriter out=response.getWriter();
	}
}
