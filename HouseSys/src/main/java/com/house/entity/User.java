package com.house.entity;

public class User {
	private int id;
    private String userName;
    private String passWord;
    private String name;
    private String phone;
    private String email;
    private boolean isAdmin;
    private boolean isInUse;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(int id, String userName, String passWord, String name,
			String phone, String email, boolean isAdmin, boolean isInUse) {
		super();
		this.id = id;
		this.userName = userName;
		this.passWord = passWord;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.isAdmin = isAdmin;
		this.isInUse = isInUse;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isAdmin() {
		return isAdmin;
	}
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public boolean isInUse() {
		return isInUse;
	}
	public void setInUse(boolean isInUse) {
		this.isInUse = isInUse;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", passWord="
				+ passWord + ", name=" + name + ", phone=" + phone + ", email="
				+ email + ", isAdmin=" + isAdmin + ", isInUse=" + isInUse + "]";
	}
    
    
}
