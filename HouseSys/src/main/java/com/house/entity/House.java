package com.house.entity;

import java.util.Date;

public class House {
	private int id;
	private int userId;
	private int typeId;
	private int streetId;
    private int floor;
    private double price;
    private Date updateTime;
    private String topic;
    private String content;
    private double longitude;
    private double latitude;
    private int state;
    private User user;
    private Type type;
    private Street street;
	public House() {
		super();
		// TODO Auto-generated constructor stub
	}
	public House(int id, int userId, int typeId, int streetId, int floor,
			double price, Date updateTime, String topic, String content,
			double longitude, double latitude, int state, User user, Type type,
			Street street) {
		super();
		this.id = id;
		this.userId = userId;
		this.typeId = typeId;
		this.streetId = streetId;
		this.floor = floor;
		this.price = price;
		this.updateTime = updateTime;
		this.topic = topic;
		this.content = content;
		this.longitude = longitude;
		this.latitude = latitude;
		this.state = state;
		this.user = user;
		this.type = type;
		this.street = street;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public int getStreetId() {
		return streetId;
	}
	public void setStreetId(int streetId) {
		this.streetId = streetId;
	}
	public int getFloor() {
		return floor;
	}
	public void setFloor(int floor) {
		this.floor = floor;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public Street getStreet() {
		return street;
	}
	public void setStreet(Street street) {
		this.street = street;
	}
	@Override
	public String toString() {
		return "House [id=" + id + ", userId=" + userId + ", typeId=" + typeId
				+ ", streetId=" + streetId + ", floor=" + floor + ", price="
				+ price + ", updateTime=" + updateTime + ", topic=" + topic
				+ ", content=" + content + ", longitude=" + longitude
				+ ", latitude=" + latitude + ", state=" + state + ", user="
				+ user + ", type=" + type + ", street=" + street + "]";
	}
    
}
