package com.house.entity;

public class Photo {
	private int id;
    private String title;
    private String fileName;
    private int hourseId;
    private House house;
	public Photo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Photo(int id, String title, String fileName, int hourseId,
			House house) {
		super();
		this.id = id;
		this.title = title;
		this.fileName = fileName;
		this.hourseId = hourseId;
		this.house = house;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getHourseId() {
		return hourseId;
	}
	public void setHourseId(int hourseId) {
		this.hourseId = hourseId;
	}
	public House getHouse() {
		return house;
	}
	public void setHouse(House house) {
		this.house = house;
	}
	@Override
	public String toString() {
		return "Photo [id=" + id + ", title=" + title + ", fileName="
				+ fileName + ", hourseId=" + hourseId + ", house=" + house
				+ "]";
	}
    
}
