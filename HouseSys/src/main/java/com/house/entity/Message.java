package com.house.entity;

import java.util.Date;

public class Message {
	private int id;
    private int formUserId;
    private int toUserId;
    private Date createTime;
    private String content;
    private boolean isRead;
    private boolean isDeleteByFormUser;
    private boolean isDeletedByToUser;
    private User user;
	public Message() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Message(int id, int formUserId, int toUserId, Date createTime,
			String content, boolean isRead, boolean isDeleteByFormUser,
			boolean isDeletedByToUser, User user) {
		super();
		this.id = id;
		this.formUserId = formUserId;
		this.toUserId = toUserId;
		this.createTime = createTime;
		this.content = content;
		this.isRead = isRead;
		this.isDeleteByFormUser = isDeleteByFormUser;
		this.isDeletedByToUser = isDeletedByToUser;
		this.user = user;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFormUserId() {
		return formUserId;
	}
	public void setFormUserId(int formUserId) {
		this.formUserId = formUserId;
	}
	public int getToUserId() {
		return toUserId;
	}
	public void setToUserId(int toUserId) {
		this.toUserId = toUserId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public boolean isRead() {
		return isRead;
	}
	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}
	public boolean isDeleteByFormUser() {
		return isDeleteByFormUser;
	}
	public void setDeleteByFormUser(boolean isDeleteByFormUser) {
		this.isDeleteByFormUser = isDeleteByFormUser;
	}
	public boolean isDeletedByToUser() {
		return isDeletedByToUser;
	}
	public void setDeletedByToUser(boolean isDeletedByToUser) {
		this.isDeletedByToUser = isDeletedByToUser;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Message [id=" + id + ", formUserId=" + formUserId
				+ ", toUserId=" + toUserId + ", createTime=" + createTime
				+ ", content=" + content + ", isRead=" + isRead
				+ ", isDeleteByFormUser=" + isDeleteByFormUser
				+ ", isDeletedByToUser=" + isDeletedByToUser + ", user=" + user
				+ "]";
	}
    
}
