package com.house.entity;

public class Street {
	private int id;
    private String name;
    private int districtId;
    private District district;
	public Street() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Street(int id, String name, int districtId, District district) {
		super();
		this.id = id;
		this.name = name;
		this.districtId = districtId;
		this.district = district;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDistrictId() {
		return districtId;
	}
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}
	public District getDistrict() {
		return district;
	}
	public void setDistrict(District district) {
		this.district = district;
	}
	@Override
	public String toString() {
		return "Street [id=" + id + ", name=" + name + ", districtId="
				+ districtId + ", district=" + district + "]";
	}
    
}
