package com.house.dao;

import java.util.List;

import com.house.entity.House;

public interface ViolationHouseDao {
	public List<House> getAll();
	public List<House> getById(int id);
	public void delete(int id);

}
