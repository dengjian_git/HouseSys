package com.house.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.house.entity.District;

public interface DistrictDao {
	//增加
	public void add(District district);
	//删除
	public void delete(int id);
	//查询所有
	public List<District> getAll();
	//根据id查询
	public District getById(int id);
	//分页查询
	public List<District> getPageIndex(@Param("name")String name ,@Param("pageSize") Integer pageSize,@Param("startIndex") Integer startIndex);
	//修改
	public void update(District district);
	public int getRowsCount(@Param("id") Integer id,@Param("name") String name);
}
