package com.house.convert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

//类型转换方法 
public class UtilDateConverter implements Converter<String, Date>{

	private String [] formates;
	
	public void setFormates(String[] formates) {
		this.formates = formates;
	}

	public Date convert(String value) {
		if(value==null||value.isEmpty()){
			return null;
		}else{
			for(String str:formates){
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str);
				try {
					return simpleDateFormat.parse(value);
				} catch (ParseException e) {
					continue;
				}
			}
			throw new RuntimeException("格式不正确");
		}
	}

}
