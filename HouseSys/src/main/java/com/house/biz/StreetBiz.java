package com.house.biz;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.house.entity.Street;

public interface StreetBiz {
	//增加
	public void add(Street street);
	//删除
	public void delete(int id);
	//查询所有
	public List<Street> getAll();
	//根据id查询
	public Street getById(int id);
	//分页查询
	public List<Street> getPageIndex(@Param("name")String name ,@Param("pageSize") Integer pageSize,@Param("startIndex") Integer startIndex);
	//修改
	public void update(Street street);
	public int getRowsCount(@Param("id") Integer id,@Param("name") String name);
}
