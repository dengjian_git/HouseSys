package com.house.biz.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.house.biz.DistrictBiz;
import com.house.dao.DistrictDao;
import com.house.entity.District;
@Service("districtBiz")
public class DistrictBizImpl implements DistrictBiz{
	@Autowired
	private DistrictDao districtDao;

	public void add(District district) {
		// TODO Auto-generated method stub
		districtDao.add(district);
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		districtDao.delete(id);
	}

	public List<District> getAll() {
		// TODO Auto-generated method stub
		return districtDao.getAll();
	}

	public District getById(int id) {
		// TODO Auto-generated method stub
		return districtDao.getById(id);
	}

	public List<District> getPageIndex(String name,Integer pageSize, Integer startIndex) {
		// TODO Auto-generated method stub
		return districtDao.getPageIndex(name,pageSize, startIndex);
	}

	public void update(District district) {
		// TODO Auto-generated method stub
		districtDao.update(district);
	}

	public int getRowsCount(Integer id, String name) {
		// TODO Auto-generated method stub
		return districtDao.getRowsCount(id, name);
	}
	
}
