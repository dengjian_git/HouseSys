package com.house.biz.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.house.biz.ViolationHouseBiz;
import com.house.dao.ViolationHouseDao;
import com.house.entity.House;
@Service("violationHouseBiz")
public class ViolationHouseBizImpl implements ViolationHouseBiz{
	@Autowired
	private ViolationHouseDao vhDao;
	public List<House> getAll() {
		return vhDao.getAll();
	}

	public List<House> getById(int id) {
		// TODO Auto-generated method stub
		return vhDao.getById(id);
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		vhDao.delete(id);
	}

}
