package com.house.biz.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.house.biz.StreetBiz;
import com.house.dao.StreetDao;
import com.house.entity.Street;
@Service("streetBiz")
public class StreetBizImpl implements StreetBiz {
	@Autowired
	private StreetDao streetDao;
	public void add(Street street) {
		// TODO Auto-generated method stub
		streetDao.add(street);
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		streetDao.delete(id);
	}

	public List<Street> getAll() {
		// TODO Auto-generated method stub
		return streetDao.getAll();
	}

	public Street getById(int id) {
		// TODO Auto-generated method stub
		return streetDao.getById(id);
	}

	public List<Street> getPageIndex(String name,Integer pageSize, Integer startIndex) {
		// TODO Auto-generated method stub
		return streetDao.getPageIndex(name,pageSize, startIndex);
	}

	public void update(Street street) {
		// TODO Auto-generated method stub
		streetDao.update(street);
	}

	public int getRowsCount(Integer id, String name) {
		// TODO Auto-generated method stub
		return streetDao.getRowsCount(id, name);
	}

}
