package com.house.biz.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.house.biz.TypeBiz;
import com.house.dao.TypeDao;
import com.house.entity.Type;
@Service("typeBiz")
public class TypeBizImpl implements TypeBiz{
	@Autowired
	private TypeDao typeDao;
	public void add(Type type) {
		// TODO Auto-generated method stub
		typeDao.add(type);
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		typeDao.delete(id);
	}

	public List<Type> getAll() {
		// TODO Auto-generated method stub
		return typeDao.getAll();
	}

	public Type getById(int id) {
		// TODO Auto-generated method stub
		return typeDao.getById(id);
	}

	public List<Type> getPageIndex(String name,Integer pageSize, Integer startIndex) {
		// TODO Auto-generated method stub
		return typeDao.getPageIndex(name,pageSize, startIndex);
	}

	public void update(Type type) {
		// TODO Auto-generated method stub
		typeDao.update(type);
	}

	public int getRowsCount(Integer id, String name) {
		// TODO Auto-generated method stub
		return typeDao.getRowsCount(id, name);
	}

}
