package com.house.biz;

import java.util.List;

import com.house.entity.House;

public interface ViolationHouseBiz {
	public List<House> getAll();
	public List<House> getById(int id);
	public void delete(int id);
}
