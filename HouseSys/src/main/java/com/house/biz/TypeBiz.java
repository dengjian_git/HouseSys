package com.house.biz;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.house.entity.Type;

public interface TypeBiz {
	//增加
	public void add(Type type);
	//删除
	public void delete(int id);
	//查询所有
	public List<Type> getAll();
	//根据id查询
	public Type getById(int id);
	//分页查询
	public List<Type> getPageIndex(@Param("name")String name ,@Param("pageSize") Integer pageSize,@Param("startIndex") Integer startIndex);
	//修改
	public void update(Type type);
	public int getRowsCount(@Param("id") Integer id,@Param("name") String name);
}
